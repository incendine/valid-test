var employee = new Array();

window.onload = function () {
    this.loadTable();
};

function loadTable() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", 'http://localhost:8080/clienteWS/', false); // false for synchronous request
    xmlHttp.send(null);

    const payload = JSON.parse(xmlHttp.responseText);

    this.employee = payload;

    var tablecontents = "";
    for (var i = 0; i < this.employee.length; i++) {
        tablecontents += "<tr id=row" + this.employee[i].id + ">";
        tablecontents += "<td>" + this.employee[i].id + "</td>";
        tablecontents += "<td>" + this.employee[i].nombre + "</td>";
        tablecontents += "<td>" + this.employee[i].apellido + "</td>";
        tablecontents += this.employee[i].procesado ? "<td><i style='color: green;' class='fa fa-check'></i></td>" : "<td><i style='color: red;' class='fa fa-times '></i></td>";
        tablecontents += "<td><div><input id='check" + this.employee[i].id + "' class='form-check-input' type='checkbox' id='checkboxNoLabel' value='" + this.employee[i].id + "' aria-label='...'></div></td>";

        tablecontents += "</tr>";
    }
    document.getElementById("mytablecontent").innerHTML += tablecontents;
}

function httpGet() {
    const nombre = document.getElementById("fname").value
    const apellido = document.getElementById("flastname").value
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", 'http://localhost:8080/clienteWS/save', false);
    xmlHttp.setRequestHeader("Content-Type", "application/json");
    xmlHttp.send(JSON.stringify({
        'nombre': nombre,
        'apellido': apellido,
    }));
}


function httpProcess() {
    var employeeProcess = new Array();
    for (var i = 0; i < this.employee.length; i++) {
        const seleccionado = document.getElementById("check" + this.employee[i].id).checked
        if (seleccionado) {
            const rowEmploye = 'row' + this.employee[i].id;
            var id = document.getElementById(rowEmploye).childNodes[0].textContent;
            var nombre = document.getElementById(rowEmploye).childNodes[1].textContent;
            var apellido = document.getElementById(rowEmploye).childNodes[2].textContent;
            employeeProcess.push(
                {
                    "id": id,
                    "nombre": nombre,
                    "apellido": apellido,
                    "procesado": true
                }
            )
        }
        debugger
    }
    if (employeeProcess.length > 0) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("POST", 'http://localhost:8080/clienteWS/saveall', false);
        xmlHttp.setRequestHeader("Content-Type", "application/json");
        xmlHttp.send(JSON.stringify(employeeProcess));
        location.reload();
    } else {
        alert('Debe seleccionar al menos un registro')
    }

}
