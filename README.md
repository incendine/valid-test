# Valid-Test

Proyecto de prueba para Valid para Backend, con una pagina Frontend para la interaccion grafica con los servicios back.

## Comenzando 🚀

Clonar todo el repositorio en la maquina local/

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

El proyecto necesita tener instalado Java, Maven y correrlo con:
```
Eclipse
```

### Instalación 🔧

Importar el proyecto en Eclipse


## Despliegue 📦

El servidor arranca en el puerto 8080

## Construido con 🛠️

* [Spring boot](https://spring.io/projects/spring-boot) - El framework Spring usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias


## Autores ✒️


* **Carlos D. Escobar** - *Trabajo total* - [incendine](https://gitlab.com/incendine)


## Expresiones de Gratitud 🎁

* Gracias por esta oportunidad📢
* Un café ☕ con juegos de mesa a todos los que me apoyen en los que me gusta. 
* Gracias🤓.
