/**
 * Author:  Charly
 * Created: Feb 27, 2021
 */

DROP TABLE IF EXISTS cliente;

CREATE TABLE cliente (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nombre VARCHAR(250) NOT NULL,
  apellido VARCHAR(250) NOT NULL,
  procesado BOOLEAN DEFAULT false
);

INSERT INTO cliente (nombre, apellido) VALUES
  ('Charly', 'Escobar');