/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.valid.carlos.ws;

import co.valid.carlos.model.dto.ClienteDTO;
import co.valid.carlos.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 *
 * @author Charly
 */
@RestController
@CrossOrigin
@RequestMapping(path = "/clienteWS")
public class ClienteWS {

    @Autowired
    ClienteService clienteService;

    @GetMapping(path = "/")
    public @ResponseBody
    ResponseEntity<?> consultarClientes() {
        try {
            return new ResponseEntity<>(clienteService.consultarClientes(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/save")
    public @ResponseBody
    ResponseEntity<?> guardarCliente(@RequestBody ClienteDTO nuevoCliente) {
        try {
            return new ResponseEntity<>(clienteService.saveCliente(nuevoCliente), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PostMapping(path = "/saveall")
    public @ResponseBody
    ResponseEntity<?> guardarClientes(@RequestBody List<ClienteDTO> nuevosClientes) {
        try {
        	clienteService.saveClientes(nuevosClientes);
            return new ResponseEntity<>("Ok", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
