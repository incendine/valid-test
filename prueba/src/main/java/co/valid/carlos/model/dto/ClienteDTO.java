package co.valid.carlos.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import co.valid.carlos.model.Cliente;

/**
 *
 * @author Charly
 */
public class ClienteDTO {

	private Integer id;

	private String nombre;

	private String apellido;

	private boolean procesado;

	public ClienteDTO() {
	}

	public ClienteDTO(Cliente clienteModel) {
		if (clienteModel != null) {
			this.id = clienteModel.getId();
			this.nombre = clienteModel.getNombre();
			this.apellido = clienteModel.getApellido();
			this.procesado = clienteModel.isProcesado();
		}
	}

	@JsonIgnore
	public Cliente getCliente() {
		Cliente cliente = new Cliente();
		cliente.setId(this.getId());
		cliente.setNombre(this.getNombre());
		cliente.setApellido(this.getApellido());
		cliente.setProcesado(this.isProcesado());
		return cliente;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public boolean isProcesado() {
		return procesado;
	}

	public void setProcesado(boolean procesado) {
		this.procesado = procesado;
	}

}
