/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.valid.carlos.service;

import co.valid.carlos.model.dto.ClienteDTO;

import java.util.List;

/**
 *
 * @author Charly
 */
public interface ClienteService {

    public List<ClienteDTO> consultarClientes();

    public ClienteDTO saveCliente(ClienteDTO nuevoCliente);
    
    public void saveClientes(List<ClienteDTO> nuevoCliente);

}
