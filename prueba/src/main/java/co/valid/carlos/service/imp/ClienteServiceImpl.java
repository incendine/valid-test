package co.valid.carlos.service.imp;

import co.valid.carlos.dao.ClienteDAO;
import co.valid.carlos.model.Cliente;
import co.valid.carlos.model.dto.ClienteDTO;
import co.valid.carlos.service.ClienteService;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	ClienteDAO clienteDAO;

	@Override
	public List<ClienteDTO> consultarClientes() {
		List<Cliente> clientes = clienteDAO.findAll();
		List<ClienteDTO> clientesDTO = new ArrayList<>();
		clientes.forEach(cli -> clientesDTO.add(new ClienteDTO(cli)));
		return clientesDTO;
	}

	@Override
	public ClienteDTO saveCliente(ClienteDTO nuevoCliente) {
		return new ClienteDTO(clienteDAO.save(nuevoCliente.getCliente()));
	}

	@Override
	public void saveClientes(List<ClienteDTO> nuevoCliente) {
		List<Cliente> clientes = new ArrayList<>();
		nuevoCliente.forEach(cli -> clientes.add(cli.getCliente()));
		clienteDAO.saveAll(clientes);
	}

}
