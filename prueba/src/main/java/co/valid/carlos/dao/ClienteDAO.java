/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.valid.carlos.dao;

import co.valid.carlos.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Charly
 */
@Repository
public interface ClienteDAO extends JpaRepository<Cliente, Integer>  {
    
}
